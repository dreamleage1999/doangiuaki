
   
    const api="https://api.themoviedb.org/3/search/movie?api_key=ab75b4eeca2724997bcd8744a3cac5b7";
    const api1="https://api.themoviedb.org/3/search/person?api_key=ab75b4eeca2724997bcd8744a3cac5b7";
    //const upcoming="https://api.themoviedb.org/3/movie/upcoming?api_key=ab75b4eeca2724997bcd8744a3cac5b7";
    const first_img="https://image.tmdb.org/t/p/w500";
    var lastPage = 1;
    var lastPageRev = 1;
    var typeFind = 1;
    //change state of selection
   function ChangeTypeSearch(type)
    {
      if(type==1)
      {
        typeFind=1;
        document.getElementById('type-movies').style.color="blue";
        document.getElementById('type-actor').style.color="black";
      }
      if(type==2)
      {
        typeFind=2;
        document.getElementById('type-actor').style.color="blue";
        document.getElementById('type-movies').style.color="black";
      }
     //console.log(typeFind);
    }
    //get info of actor form id
  async function getInforFromId(id)
  {
    const strReq=`https://api.themoviedb.org/3/movie/${id}?api_key=ab75b4eeca2724997bcd8744a3cac5b7`;
    const response=await fetch(strReq);
    let data=await response.json();
    return data;
  }
  //get name of genres
  async function getGenres(id)
  {
    const strReq=`https://api.themoviedb.org/3/genre/movie/list?api_key=ab75b4eeca2724997bcd8744a3cac5b7`;
    const response=await fetch(strReq);
    let data=await response.json();
    data=data.genres;
    console.log(data);
    let res=[];
    for(let i=0;i<id.length;i++)
    {
    let temp=data.find(function(res)
    {
      return res.id==id[i];
    });
    console.log(id[i]);
    res[i]=temp.name;
  }
  return res;
  }
  //show actor'infomation 
   async function showInfor(id)
  {
    Loading();
   const reqStr=`https://api.themoviedb.org/3/person/${id}?api_key=ab75b4eeca2724997bcd8744a3cac5b7`;
    const response=await fetch(reqStr);
    const data=await response.json();
   const strReq1=`https://api.themoviedb.org/3/search/person?api_key=ab75b4eeca2724997bcd8744a3cac5b7&query=${data.name}`;
   const response1=await fetch(strReq1);
   const data1=await response1.json();
   const n_data2=data1.total_pages;

   let arr_know=[];
      for(let i=1;i<=n_data2;i++)
      {
        let temp=await loadDataFromLink(`${strReq1}&page=${i}`);
       
       for(let i=0;i<temp.length;i++)
       {
         if(temp[i].known_for.length!=0)
         {
           //console.log(temp[i].known_for);
           arr_know=arr_know.concat(temp[i].known_for);
         }
       }
      }
     arr_know=arr_know.filter(function(res)
     {
       return res.poster_path!=null && res.media_type=="movie";
     })

     //console.log(arr_know);
     arr_know=arr_know.slice(0,5);
     
    $("#main").empty();
    $("#main").append(`


    <!-- Page Content -->
<div class="container">

  <!-- Portfolio Item Heading -->
  <h1 class="my-4" style="color:orange">${data.name}
    <small></small>
  </h1>

  <!-- Portfolio Item Row -->
  <div class="row">

    <div class="col-md-7">
      <img class="img-fluid" src="${data.profile_path==null?'/assets/images/default-avatar.png':first_img + data.profile_path}" alt="${data.name}" title="${data.name}">
    </div>

    <div class="col-md-5">
      <h3 class="my-3" style="color:orange">Biography</h3>
      <p style="color:white">${data.biography}</p>
      <h3 class="my-3" style="color:orange">Details</h3>
      <ul style="color:white">
      <li><b>Full name:</b> ${data.name}</li>
        <li><b>Birthday:</b> ${data.birthday}</li>
         <li><b>Place of birth:</b> ${data.place_of_birth}</li>
         <li><b>Gender:</b> ${data.gender==2?"Male":"Female"}</li>
         <li><b>Known for department:</b> ${data.known_for_department}</li>
         <li><b>Another Name:</b> ${data.also_known_as!=""?data.also_known_as:"null"}</li>
      </ul>
    </div>

  </div>
  <!-- /.row -->

  <!-- Related Projects Row -->
  <h3 class="my-4" style="color:orange">Relative Movies</h3>

  <ul class="list-unstyled" id="listrelate" style="color:white">
  
  
  
</ul>
</div>

    `);
for(let i of arr_know)
{
  const res=await getInforFromId(i.id);
  let arr_genres=res.genres.map(function(res)
  {
    return res.name;
  });
  let produc_countries=res.production_countries.map(function(res)
  {
    return res.name;
  });
  $('#listrelate').append(`
  <li class="media mt-4" onclick="LoadDetail('${i.id}')">
    <img src="https://image.tmdb.org/t/p/w200${i.poster_path}" class="mr-3 " alt="${i.title}" title="${i.title}">
    <div class="media-body">
      <h4 class="mt-0 mb-1" style="color:orange">${i.title}</h4>
     <p><b>Overview:</b> ${i.overview}</p>
     <p><b>Run time:</b> ${res.runtime+" minutes"}</p>
     <p><b>Released date:</b> ${i.release_date}</p>
     <p><b>Genres:</b>${arr_genres}</p>
     <p><b>Production countries:</b>${produc_countries}</p>
     
    </div>
  </li>
  `);
}
    //console.log(data);
  }

  //Load spinner
    function Loading()
    {

      $('#main').empty();
     //$('.pagination1').empty();
     
      $('#main').append(`
      <div class="spinner-grow" role="status" style="color:orange">
      <span class="sr-only">Loading...</span>
     </div>
      `);
    }
    //Render details of movie
    async function fillMovie(m)
    {
      //console.log(m.production_countries);
      let genre_arr=[];
      genre_arr=m.genres.map(x=>{return x.name;});
      let country_arr=[];
      country_arr=m.production_countries.map(x=>{return x.name;});
     // console.log(country_arr);
      const reqMember=`https://api.themoviedb.org/3/movie/${m.id}/credits?api_key=ab75b4eeca2724997bcd8744a3cac5b7`;
      const data=await fetch(reqMember);
      const member=await data.json();
      //console.log(member);
       //console.log(dataMember.cast);
       const arr_Actors=member.cast.slice(0,6);
      const Actors=[];
      for(let i=0;i<arr_Actors.length;i++)
      {
        let temp1={};
        temp1.character=arr_Actors[i].character;
        temp1.name=arr_Actors[i].name;
        temp1.id=arr_Actors[i].id;
        Actors[i]=temp1;
      }
      //console.log(arr_Actors);
      let info_actors=``;
      for(let i=0;i<Actors.length;i++)
      {
        if(i==Actors.length-1)
        {
        let infor_mem=`<a href="#" alt="${Actors[i].name+" as "+Actors[i].character}" title="${Actors[i].name+" as "+Actors[i].character}" onclick="showInfor('${Actors[i].id}')">${Actors[i].name}</a> `;
        info_actors+=infor_mem;
        }
        else
        {
          let infor_mem=`<a href="#" alt="${Actors[i].name+" as "+Actors[i].character}" title="${Actors[i].name+" as "+Actors[i].character}" onclick="showInfor('${Actors[i].id}')">${Actors[i].name}</a>, `;
        info_actors+=infor_mem;
        }
      }
       //console.log(info_actors);
       const Directors=member.crew.filter(x=>{return x.job=="Director"});
       let info_directors=``;
       //console.log(Directors);
       for(let i=0;i<Directors.length;i++)
      {
        if(i==Directors.length-1)
        {
        let infor_mem=`<a href="#" alt="${Directors[i].name}" title="${Directors[i].name}" onclick="showInfor('${Directors[i].id}')">${Directors[i].name}</a> `;
        info_directors+=infor_mem;
        }
        else
        {
          let infor_mem=`<a href="#" alt="${Directors[i].name}" title="${Directors[i].name}" onclick="showInfor('${Directors[i].id}')">${Directors[i].name}</a>, `;
        info_directors+=infor_mem;
        }
      }

       const reqReview=`https://api.themoviedb.org/3/movie/${m.id}/reviews?api_key=ab75b4eeca2724997bcd8744a3cac5b7`;
       const data1=await fetch(reqReview);
       const review=await data1.json();
       //console.log(review);
      $("#main").empty();
     
      $("#main").append(`
      <!-- Page Content -->
<div class="container">

  <!-- Portfolio Item Heading -->
  <h1 class="my-4" style="color:orange">${m.title}
    <small>${m.release_date}</small>
  </h1>

  <!-- Portfolio Item Row -->
  <div class="row">

    <div class="col-md-7">
      <img class="img-fluid" src="${first_img + m.poster_path}" alt="${m.title}">
    </div>

    <div class="col-md-5" style="color:white">
      <h3 class="my-3" style="color:orange">Overview</h3>
      <p>${m.overview}</p>
      <h3 class="my-3" style="color:orange">Details</h3>
      <ul style="color:white">
      <li><b>Title:</b> ${m.title}</li>
        <li><b>Released date:</b> ${m.release_date}</li>
         <li><b>Genres:</b> ${genre_arr}</li>
         <li><b>Director:</b> ${info_directors}</li>
         <li><b>Actors:</b> ${info_actors}</li>
         <li><b>Runtime:</b> ${m.runtime+" minutes"}</li>
         <li><b>Vote Average:</b> ${m.vote_average}</li>
         <li><b>Production Country:</b> ${country_arr}</li>
      </ul>
    </div>

  </div>
  <!-- /.row -->

  <!-- Related Projects Row -->
  <h3 class="my-4" style="color:orange">Actors/Actress</h3>

  <div class="d-flex" id="link" style="color:white">

   

  </div>
  <!-- /.row -->


  



  <h2 class="text-left" style="color:orange">Reviews</h2>
  <div class="review_list"></div>
  <div class="pagination_review"></div>
</div>
<!-- /.container -->
      `);

      for(const m of arr_Actors)
    {
        //console.log(m.name+" as "+m.character);
      if(m.profile_path!=null)
      {
      $('#link').append(`
      
      <div class="col-2" onclick="showInfor('${m.id}')"><img src="${first_img+m.profile_path}" alt='${m.name+" as "+m.character}' class="img-thumbnail" title='${m.name+" as "+m.character}' >
        <p class="ml-4" >${m.character}</p>
      </div>
     
   `);
      }
      else
      {
        $('#link').append(`
      <div class="col-2" onclick="showInfor('${m.id}')" ><img src="/assets/images/default-avatar.png" alt='${m.name+" as "+m.character}' title='${m.name+" as "+m.character}' class="img-thumbnail">
        <p class="ml-4">${m.character}</p>
      </div>
      
   `);
      }
    }




    const total_page=review.total_pages;
    lastPageRev=1;
    loadPageReivew(m.id,1);
    addPaginationReview(total_page,m.id);

    }

    async function loadPageReivew(movie_id,page)
{
    $('.review_list').empty();
    const response_1 = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/reviews?api_key=ab75b4eeca2724997bcd8744a3cac5b7&page=${page}`);
    const review = await response_1.json();
   
    const arr_review = review.results;

    for(const m of arr_review)
    {
        $('.review_list').append(`
        
        <div class="container">
	
	
        	<div class="card">
        	    <div class="card-body">
        	        <div class="row">
                	    <div class="col-md-2">
                	        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                	        <p class="text-secondary text-center"></p>
                	    </div>
                	    <div class="col-md-10">
                	        <p>
                	            <a class="float-left" href="${m.url}"><strong>${m.author}</strong></a>
                	            <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                	            <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                	            <span class="float-right"><i class="text-warning fa fa-star"></i></span>
        
                	       </p>
                         <div class="clearfix"></div>
                         
                        <p>${m.content}</p>
        
                
                         
                         
                      <p>
                	            <a class="float-right btn btn-outline-primary ml-2"> <i class="fa fa-reply"></i> Reply</a>
                	            <a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>
                	       </p>
                	    </div>
        	        </div>
                  
        	    </div>
        	</div>
        </div>
        `)
    }
    if(arr_review.length>0)
    {
    document.getElementById(`page_review_link${lastPageRev}`).style.backgroundColor = "white";
    document.getElementById(`page_review_link${page}`).style.backgroundColor = "gray";
    lastPageRev = page;
    }
}

// Pagination for Review
async function addPaginationReview(pages, movie_id) {

    $('.pagination_review').empty();

    $('.pagination_review').append(`<a href="" style="width: 50px; text-align:center">&laquo;</a>`);

    if (typeFind == 1) {
        
        for (let i=1; i <= pages; i++) {
            $('.pagination_review').append(`
            <a href="#" id="page_review_link${i}" style="width: 50px; text-align:center" onclick="loadPageReview(${movie_id},'${i}')">${i}</a>`)
        }
    }
    else {
        
        for (let i=1; i <= pages; i++) {
            $('.pagination_review').append(`
            <a href="#" id="page_review_link${i}" style="width: 50px; text-align:center" onclick="loadPageReview(${movie_id},'${i}')">${i}</a>`)
        }
    }

    $('.pagination_review').append(`<a href="" style="width: 50px; text-align:center" >&raquo;</a>`)
}







     function getInforMember()
    {
  const reqMember='https://api.themoviedb.org/3/movie/384018/credits?api_key=ab75b4eeca2724997bcd8744a3cac5b7';
    const data1=fetch(reqMember).then(function(res){return res.json()});
 // const member=data1.json();
  // const res={cast:member.cast,crew:member.crew};
  // console.log(res);
  console.log(data1);
  return data1;
    }
    async function LoadDetail(m)
    {
      
      $('.pagination1').empty();
      //const reqStr=`${api}&i=${m}`;
      const reqStr=`https://api.themoviedb.org/3/movie/${m}?api_key=ab75b4eeca2724997bcd8744a3cac5b7`;
    Loading();

      const data=await fetch(reqStr);
    
      const movie=await data.json();
    // const data1=getInforMember().then(function(res){
    //   console.log(res);
    //   return res;});
    //   console.log(data1);
     fillMovie(movie);
    }
    

    async function getIMDB(id)
    {
      const reqStr=`https://api.themoviedb.org/3/movie/${id}?api_key=ab75b4eeca2724997bcd8744a3cac5b7`;
      const response=await fetch(reqStr);
      const data=await response.json();
      let temp={};
      temp.imdb_id=data.imdb_id;
      temp.runtime=data.runtime;
      return temp;
    }
    async function getRate(imdb_id)
    {
      const reqStr=`http://www.omdbapi.com/?i=${imdb_id}&apikey=b1c3d9cc`;
      const response=await fetch(reqStr);
      const data=await response.json();
      return data.Rated;
    }
    async function fillMovies(ms,ms1)
    {
     
      $("#main").empty();
      for(const m of ms )
      {
        
        const imdbID=await getIMDB(m.id);
        let rate=await getRate(imdbID.imdb_id);
        if(rate==undefined)
        {
          rate="Not Rated";
        }
        //console.log(rate);
        if(m.poster_path!=null)
        {
        $("#main").append(`
        <div class="col-md-4 py-1 mb-3">
            <div class="card h-100 shadow" style="width: 22rem;">
                 <img src="${first_img}${m.poster_path}" class="card-img-top" alt="${m.title}" title="${m.title}" onclick="LoadDetail('${m.id}')"> 
                <div class="card-body">
                  <h5 class="card-title">${m.title}</h5>
                  <p class="card-text">${rate}</p>
                  <p class="card-text">${imdbID.runtime+" minutes"}</p>
               </div>
                </div>
              </div>
        `)
        }
      }
      //console.log(ms1);
      if(ms1.length!=0)
      {
        console.log("human ok");
      
      for(const m of ms1)
      {
       
        const arr_know=m.known_for;
        const arr_temp=[];
        for (const i of arr_know)
        {
          if(i.media_type=="movie" && i.poster_path!=null)
          {
          const temp={};
          temp.poster_path=i.poster_path;
          temp.title=i.title;
          temp.id=i.id;
          arr_temp.push(temp);
          }
        }
        console.log(arr_temp);        
        for(const i of arr_temp)
        {
          const imdbID=await getIMDB(i.id);
        let rate=await getRate(imdbID.imdb_id);
       
        if(rate==undefined)
        {
          rate="Not Rated";
        }
        $("#main").append(`
        <div class="col-md-4 py-1 mb-3">
            <div class="card h-100 shadow" style="width: 22rem;">
                 <img src="${first_img}${i.poster_path}" class="card-img-top" alt="${i.title}" title="${i.title}" onclick="LoadDetail('${i.id}')"> 
                <div class="card-body">
                  <h5 class="card-title">${i.title}</h5>
                  <p class="card-text">${rate}</p>
                  <p class="card-text">${imdbID.runtime+" minutes"}</p>
               </div>
                </div>
              </div>
        `);
        }
      }
    
      
    }
    }

//load data from link
    async function loadDataFromLink(link)
    {
      const response=await fetch(link);
      const data=await response.json();
      const results=data.results;
      return results;
    }
    
    //render film when user search
    async function eventSubmit(e)
    {
      e.preventDefault();
      Loading();  
      $('.pagination1').empty();
      const strSearch=$('form input').val();
      if(typeFind==1)
      {
        const response=await fetch(`${api}&query=${strSearch}`);
        const data=await response.json();
        let total_pages=data.total_pages;
        lastPage=1;
        RenderMovieByName(1,strSearch);
       let temp= setTimeout(pagiSearch,2000,strSearch,total_pages);
      }
      if(typeFind==2)
      {
        const response=await fetch(`${api1}&query=${strSearch}`);
       // console.log(`${api1}&query=${strSearch}`);
        const data=await response.json();
        let total_pages=data.total_pages;
        lastPage=1;
        RenderMovieByActorName(1,strSearch);
       let temp= setTimeout(pagiSearch,2000,strSearch,total_pages);
      }
    }
   
     
     //loading before show film
    function RenderMovieByActorName(page,val)
    {
      Loading();
      
      let id=setTimeout(RenderFilm1,2000,page,val);
    }
    //loading before show film
    function RenderMovieByName(page,val)
    {
      Loading();
      
      let id=setTimeout(RenderFilm,2000,page,val);
    }
    //show film with actors name search
    async function RenderFilm1(page,name)
    {
      Loading();
      $('#main').empty();
      let dem=0;
      const response = await fetch(`${api1}&query=${name}&page=${page}`);
      const data = await response.json();
      const arr_movies = data.results;
      for (const m of arr_movies) {
        const arr_known_for = m.known_for;
        let arr_temp=[];
          for (const i of arr_known_for)
          {
            if(i.media_type=="movie" && i.poster_path!=null)
            {
            
            arr_temp.push(i);
            dem++;
            }
          }
        
         
          //console.log(arr_temp);        
          for(const i of arr_temp)
          {
            if(i.poster_path!=null)
            {
              const imdbID=await getIMDB(i.id);
              let rate=await getRate(imdbID.imdb_id);
              if(rate==undefined)
          {
            rate="Not Rated";
          }
          $("#main").append(`
          <div class="col-md-4 py-1 mb-3">
              <div class="card h-100 shadow" style="width: 22rem;">
                   <img src="${first_img}${i.poster_path}" class="card-img-top" alt="${i.title}" title="${i.title}" onclick="LoadDetail('${i.id}')"> 
                  <div class="card-body">
                  <h5 class="card-title">${i.title}</h5>
                  <p class="card-text">${imdbID.runtime+" minutes"}</p>
                  <p class="card-text" style="color: black;">${i.release_date}</p>  
                  <p class="card-text">${rate}</p>
                 <p class="card-text"  style="color: blue;">${i.vote_average}&#9733</p>
                 </div>
                  </div>
                </div>
          `);
        
        }
          }
        
      
    }
    if(dem!=0 && arr_movies.length>0)
    {
    document.getElementById(`page_link${lastPage}`).style.backgroundColor = "white";
    document.getElementById(`page_link${page}`).style.backgroundColor = "gray";
    lastPage = page;
    }
    }
    //show film with movie name search
    async function RenderFilm(page, name) {

      Loading();
     
  
      const response = await fetch(`${api}&query=${name}&page=${page}`);
      const data = await response.json();
      const arr_movies = data.results;
      let dem=0,dem1=0;
      let arr1=[];
    for(const m of arr_movies)
    {
      if(m.poster_path!=null)
      {
        const imdbID=await getIMDB(m.id);
        let rate=await getRate(imdbID.imdb_id);
        if(rate==undefined)
        {
          rate="Not Rated";
        }
        let vartemp={};
        vartemp.runtime=imdbID.runtime;
        vartemp.rate=rate;
        arr1.push(vartemp);
      }
    }
    $('#main').empty();
      for (const m of arr_movies) {
         
        if(m.poster_path!=null)
        {
          $('#main').append(`
          <div class="col-md-4 py-1 mb-3">
          <div class="card h-100 shadow" style="width: 22rem;">
               <img src="${first_img}${m.poster_path}" class="card-img-top" alt="${m.title}" title="${m.title}" onclick="LoadDetail('${m.id}')"> 
              <div class="card-body">
                <h5 class="card-title">${m.title}</h5>
                <p class="card-text">${arr1[dem1].runtime+" minutes"}</p>
                <p class="card-text" style="color: black;">${m.release_date}</p>  
                <p class="card-text">${arr1[dem1].rate}</p>
               <p class="card-text"  style="color: blue;">${m.vote_average}&#9733</p>
             </div>
              </div>
            </div>
          `);
          dem1++;
        }
      }
      if(arr_movies.length>0)
      {
      document.getElementById(`page_link${lastPage}`).style.backgroundColor = "white";
      document.getElementById(`page_link${page}`).style.backgroundColor = "gray";
      lastPage = page;
      }
  }



    async function pagiSearch(val,total_pages) {

      $('.pagination1').empty();

      $('.pagination1').append(`<a href="#" style="width: 50px; text-align:center" id="#pag1">&laquo;</a>`);
  
      if (typeFind == 1) {
          
          for (let i=1; i <= total_pages; i++) {
              $('.pagination1').append(`
              <a href="#" id="page_link${i}" style="width: 50px; text-align:center" onclick="RenderMovieByName(${i},'${val}')">${i}</a>`)
          }
      }
      if(typeFind==2) 
      {
         
          for (let i=1; i <= total_pages; i++) {
              $('.pagination1').append(`
              <a href="#" id="page_link${i}" style="width: 50px; text-align:center" onclick="RenderMovieByActorName(${i},'${val}')">${i}</a>`)
          }
      }
  
      $('.pagination1').append(`<a href="#" style="width: 50px; text-align:center" id="#pag2">&raquo;</a>`)
  }

    

   

   
    //Pagination upcoming
    async function pagiUpComing(pages)
{
    $('.pagination1').empty();

    $('.pagination1').append(`<a href="#" style="width: 50px; text-align:center" id="#pag1">&laquo;</a>`);

    if (typeFind == 1) {
        
        for (let i=1; i <= pages; i++) {
            $('.pagination1').append(`
            <a href="#" id="page_link${i}" style="width: 50px; text-align:center" onclick="UpComing(${i})">${i}</a>`)
        }
    }
    if(typeFind==2) 
    {
       
        for (let i=1; i <= pages; i++) {
            $('.pagination1').append(`
            <a href="#" id="page_link${i}" style="width: 50px; text-align:center" onclick="UpComing(${i})">${i}</a>`)
        }
    }

    $('.pagination1').append(`<a href="#" style="width: 50px; text-align:center" id="#pag2">&raquo;</a>`)
}

    
    async function UpComing1(page)
 {
  Loading();
   
   
    const response = await fetch(`https://api.themoviedb.org/3/movie/upcoming?api_key=ab75b4eeca2724997bcd8744a3cac5b7&page=${page}`);
    const data = await response.json();
    const list = data.results;
    const pages = data.total_pages;
    let dem=0,dem1=0;
    let arr1=[];
    
    
   
    for(const m of list)
    {
      if(m.poster_path!=null)
      {
        const imdbID=await getIMDB(m.id);
        let rate=await getRate(imdbID.imdb_id);
        if(rate==undefined)
        {
          rate="Not Rated";
        }
        let vartemp={};
        vartemp.runtime=imdbID.runtime;
        vartemp.rate=rate;
        arr1.push(vartemp);
      }
    }
    $('#main').empty();
    for (const m of list) {
        if(m.poster_path!=null)
        {
        $('#main').append(`<div class="col-sm-3" style="margin-bottom: 10px;">
            <div style="background: white;border-radius:1px">
                <div class="card">
                     <img class="card-img-top" alt="${m.title}" title="${m.title}" src="${first_img+m.poster_path}" width="300" height="300" onclick="LoadDetail('${m.id}')">
                     <div class="card-body" style="height: 280px">
                         <h5 class="card-title" style="font-weight: bold; color: black;" >${m.title}</h4>
                         <p class="card-text"  style="color: black;">${arr1[dem1].runtime+" minutes"}</p>
                         <p class="card-text" style="color: black;">${m.release_date}</p>  
                         <p class="card-text" style="color: black;">${arr1[dem1].rate}</p>                
                         <p class="card-text"  style="color: brown;">${m.vote_average}&#9733</p>
                        
                    </div>
                 </div>
            </div>
        </div>
        `);
        dem1++;
        }
    }
    
    
    pagiUpComing(pages);
    document.getElementById(`page_link${lastPage}`).style.backgroundColor = "white";
    document.getElementById(`page_link${page}`).style.backgroundColor = "gray";
    lastPage = page;
}

function UpComing(page)
 {
   Loading();
   let id=setTimeout(UpComing1,2000,page);
    
}

//Get upcoming
function doSomeThing(page)
{
   Loading();
   let temp=setTimeout(UpComing, 1000, page);
}
    $().ready(()=>
  {
  doSomeThing(1);
  
  });

  $().ready(()=>
  {
    $('form').on('submit',eventSubmit);
  });

